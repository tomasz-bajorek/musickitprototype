//
//  TrackTableViewCell.swift
//  MusicKitPrototype
//
//  Created by Tomasz Bajorek on 25/8/17.
//  Copyright © 2017 Outware. All rights reserved.
//

import UIKit

class TrackTableViewCell:UITableViewCell {
    static let identifier = String(describing: TrackTableViewCell.self)
    static let nib = UINib(nibName: TrackTableViewCell.identifier, bundle: nil)
}
