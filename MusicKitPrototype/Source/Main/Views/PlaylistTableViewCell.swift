//
//  PlaylistTableViewCell.swift
//  MusicKitPrototype
//
//  Created by Tomasz Bajorek on 25/8/17.
//  Copyright © 2017 Outware. All rights reserved.
//

import UIKit

class PlaylistTableViewCell:UITableViewCell {
    
    static let identifier = String(describing: PlaylistTableViewCell.self)
    static let nib = UINib(nibName: PlaylistTableViewCell.identifier, bundle: nil)
    
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
}

