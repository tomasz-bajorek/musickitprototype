//
//  MusicViewController.swift
//  MusicKitPrototype
//
//  Created by Tomasz Bajorek on 25/8/17.
//  Copyright © 2017 Outware. All rights reserved.
//
import UIKit

class MusicViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    /// The instance of `AuthorizationManager` which is responsible for managing authorization for the application.
    lazy var authorizationManager: AuthorizationManager = {
        return AuthorizationManager(appleMusicManager: self.appleMusicManager)
    }()
    
    /// The instance of `MediaLibraryManager` which manages the `MPPMediaPlaylist` this application creates.
    lazy var mediaLibraryManager: MediaLibraryManager = {
        return MediaLibraryManager(authorizationManager: self.authorizationManager)
    }()
    
    /// The instance of `AppleMusicManager` which handles making web service calls to Apple Music Web Services.
    var appleMusicManager = AppleMusicManager()
    
    /// The instance of `MusicPlayerManager` which handles media playback.
    var musicPlayerManager = MusicPlayerManager()
    
    var viewModel:MusicViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = MusicViewModel()
        
        tableView.dataSource = viewModel
        tableView.delegate = self
        tableView.register(TrackTableViewCell.nib, forCellReuseIdentifier: TrackTableViewCell.identifier)
        tableView.register(PlaylistTableViewCell.nib, forCellReuseIdentifier: PlaylistTableViewCell.identifier)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        authorizationManager.requestCloudServiceAuthorization()
        
        authorizationManager.requestMediaLibraryAuthorization()
    }
}

extension MusicViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.mode == .songs ? 120 : 80
    }
}
