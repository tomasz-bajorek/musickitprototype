//
//  MusicViewModel.swift
//  MusicKitPrototype
//
//  Created by Tomasz Bajorek on 25/8/17.
//  Copyright © 2017 Outware. All rights reserved.
//

import UIKit

enum MusicDataType {
    case songs
    case playlists
}

class MusicViewModel:NSObject {
    var mode:MusicDataType = .songs
    
}

extension MusicViewModel: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = mode == .songs ? TrackTableViewCell.identifier : PlaylistTableViewCell.identifier
        
        var cell:UITableViewCell
        switch mode {
        case .songs:
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! TrackTableViewCell
        case .playlists:
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! PlaylistTableViewCell
        }
        
        return cell
    }
}
