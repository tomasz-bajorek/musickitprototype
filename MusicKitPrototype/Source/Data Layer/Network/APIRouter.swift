//
//  APIRouter.swift
//  MusicKitPrototype
//
//  Created by Tomasz Bajorek on 25/8/17.
//  Copyright © 2017 Outware. All rights reserved.
//

import Foundation
import Alamofire

protocol APIRouter: URLRequestConvertible {
    var baseURL: URL { get }
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters? { get }
    var encoding: ParameterEncoding { get }
    var headers: [String: String]? { get }
}

extension APIRouter {
    
    var parameters: Parameters? {
        return nil
    }
    
    var headers: [String: String]? {
        return nil
    }
}

protocol MusicAPIRouter: APIRouter {}

extension MusicAPIRouter {
    
    var baseURL: URL {
        return URL(string: "https://api.music.apple.com/")!
    }
    
    var method:HTTPMethod {
        return .get
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        return try encoding.encode(urlRequest, with: parameters)
    }
    
    var encoding: ParameterEncoding {
        switch method {
        case .post:
            return JSONEncoding.default
        default:
            return URLEncoding.methodDependent
        }
    }
}

enum MusicDataAPIRouter: MusicAPIRouter {
    case recentlyPlayed
    case userStorefront
    case search(String, String, String)
    case storefrontLookup(String, String)

    var path: String {
        switch self{
        case .recentlyPlayed:
            return "/v1/me/recent/played"
        case .userStorefront:
            return "/v1/me/storefront"
        case .search(_, let countryCode, _):
            return "/v1/catalog/\(countryCode)/search"
        case .storefrontLookup(let regionCode, _):
            return "/v1/storefronts/\(regionCode)"
        }
        
    }
    
    var parameters: Parameters? {
        switch self {
        case .search(let term, _, _):
            return ["term": term.replacingOccurrences(of: " ", with: "+"),
                          "limit" : 10,
                          "types" : "songs,albums"]
        default:
            break
        }
        
        return nil
    }
    
    
    var headers: [String : String]? {
        switch self {
        case .search(_, _, let devToken):
            return ["Authorization" : "Bearer \(devToken)"]
        case .storefrontLookup(_, let devToken):
            return ["Authorization" : "Bearer \(devToken)"]
        default:
            break
        }
        
        return nil
    }
    
}

