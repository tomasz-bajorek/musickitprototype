//
//  MusicAPIClient.swift
//  MusicKitPrototype
//
//  Created by Tomasz Bajorek on 25/8/17.
//  Copyright © 2017 Outware. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

enum Result<Value, Error> {
    case success(Value)
    case failure(Error)
}

enum DataFetchError:Error {
    case error
}


typealias MusicFetchResult = Result<MediaItemsResponse, DataFetchError>
typealias MusicFetchCompletion = (_ result: MusicFetchResult) -> Void

class MusicAPIClient {
    
    func fetchDeveloperToken() -> String? {
        
        // MARK: ADAPT: YOU MUST IMPLEMENT THIS METHOD
        let developerAuthenticationToken: String? = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjQyVlc3U1o5R1cifQ.eyJpc3MiOiJZMlhOMloySEEyIiwiaWF0IjoxNTAzODkwNzcyLCJleHAiOjE1MDM4OTI1MDB9.MGOc79FiBmOz1VMuOeis2kIm3dwfTr6Z_CKtwQOE9g_6bjL7d6gZa0Soyfh7d-rI40HajLIhKOZ0kflDpgYvYQ"
        
        return developerAuthenticationToken
    }
    
    func search(term:String, countryCode:String, completion: @escaping MusicFetchCompletion) {
        
        guard let developerToken = fetchDeveloperToken() else {
            fatalError("Developer Token not configured. See README for more details.")
        }
        
        Alamofire.request(MusicDataAPIRouter.search(countryCode, term, developerToken)).responseObject { (response: DataResponse<MediaItemsResponse>) in
            switch response.result {
            case .success(let value):
            
            break
            case .failure:
                completion(.failure(.error))
            }
        }
    }
    
    func appleStorefrontLookup(regionCode: String, completion: @escaping MusicFetchCompletion) {
        
        guard let developerToken = fetchDeveloperToken() else {
            fatalError("Developer Token not configured. See README for more details.")
        }
        
        Alamofire.request(MusicDataAPIRouter.storefrontLookup(regionCode, developerToken)).responseObject { (response: DataResponse<MediaItemsResponse>) in
            switch response.result {
            case .success(let value):
                
                break
            case .failure:
                completion(.failure(.error))
            }
        }
    }
    
}
